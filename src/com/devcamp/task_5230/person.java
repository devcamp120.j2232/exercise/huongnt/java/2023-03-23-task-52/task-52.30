package com.devcamp.task_5230;
import java.util.Arrays;

public class person {
    String name; //tên người
    int age; //tuổi
    double weight; //cân nặng
    long salary; //lương
    String[] pets; 



    //khởi tạo 1 tham số name
    public person(String name){
        this.name = name;
        this.age = 18;
        this.weight = 60.5;
        this.salary = 10000000;
        this.pets = new String []{"Big Dog", "Small Cat", "Gold fish", "Red parrot"};
    }
    
    //khởi tạo với tất cả tham số
    public person(String name, int age, double weight, long salary, String[] pets) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = salary;
        this.pets = pets;
    }

      //khởi tạo k tham số
    public person() {
        this("N.T.Huong");
    }

     //khởi tạo với 3 tham số
    public person(String name, int age, double weight) {
        this(name, age, weight, 20000000, new String[] {"Big Dog", "Small cat", "Gold fish", "Red parrot"});

    }

   @Override
   public String toString(){
    return "Person [name = " + name + ", age = " + age + ", weight = " + weight + ", salary = " + salary + ", pets= " + Arrays.toString(pets) + "]";
   }
    
  
    
}
