import java.util.ArrayList;
import com.devcamp.task_5230.person;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<person> arrayList = new ArrayList<>();
        //khởi tạo person với các tham số khác nhau
        person person0 = new person();
        person person1 = new person("Devcamp");
        person person2 = new person("huong", 24, 45.5);
        person person3 = new person("Neko", 30, 45.6, 500000000, new String[]{"Little Camel"});
        
        //thêm Obj person vào danh sách
        arrayList.add(person0);
        arrayList.add(person1);
        arrayList.add(person2);
        arrayList.add(person3);

        //in ra màn hình
        for (person person: arrayList){
            System.out.println(person);
        }

    }
}
